variables:
  DOCKER_DRIVER: "overlay2"
  GIT_SUBMODULE_STRATEGY: "recursive"
  ROCKSDB_INCLUDES: "${CI_PROJECT_DIR}/ci/include"
  ROCKSDB_LIBS: "${CI_PROJECT_DIR}/ci/lib"
  R_LIBS: "${CI_PROJECT_DIR}/ci/R/library"
  R_CHECK_DIR: "${CI_PROJECT_DIR}/ci/R/logs"
  R_BUILD_ARGS: "--no-manual --no-build-vignettes"
  R_CHECK_ARGS: "--as-cran --no-manual --ignore-vignettes"
  R_INSTALL_ARGS: "--no-multiarch --with-keep.source"
  R_PKG_DEPS: "cat(remotes::local_package_deps(dependencies = TRUE))"
  _R_CHECK_CRAN_INCOMING_: "false"
  _R_CHECK_FORCE_SUGGESTS_: "true"

cache:
  paths:
    - ${R_LIBS}

.build:rocksdb:
  variables:
    CCACHE_BASEDIR: "${CI_PROJECT_DIR}"
    CCACHE_DIR: "${CI_PROJECT_DIR}/ci/ccache"
    CXXFLAGS: "-O2 -pipe"
    LDFLAGS: "-pthread -ldl -lz -lbz2 -lsnappy -llz4 -lzstd"
    USE_RTTI: "1"
  stage: build
  before_script:
    - export PATH="/usr/lib/ccache:$PATH"
    - apt-get update
    - apt-get install -y make g++ ccache
    - apt-get install -y libgflags-dev zlib1g-dev libbz2-dev liblz4-dev libzstd-dev libsnappy-dev
  script:
    - mkdir -p ${CCACHE_DIR}
    - mkdir -p ${ROCKSDB_LIBS}
    - mkdir -p ${ROCKSDB_INCLUDES}
    - make -C src/rocksdb -j $(nproc) shared_lib
    - cp -a src/rocksdb/librocksdb.so* ${ROCKSDB_LIBS}
    - cp -a src/rocksdb/include/. ${ROCKSDB_INCLUDES}
  artifacts:
    paths:
      - ${ROCKSDB_INCLUDES}
      - ${ROCKSDB_LIBS}
  cache:
    key: ${CI_JOB_NAME}
    paths:
      - ${CCACHE_DIR}

.test:rpkg:
  stage: test
  script:
    - R CMD build ${R_BUILD_ARGS} .
    - R CMD check ${R_CHECK_ARGS} -o ${R_CHECK_DIR} $(ls -1t *.tar.gz | head -n 1)
  artifacts:
    paths:
      - ${R_CHECK_DIR}/*.Rcheck
    name: ${CI_JOB_NAME}-${CI_COMMIT_REF_NAME}
    when: always

before_script:
  - mkdir -p ${ROCKSDB_LIBS}
  - mkdir -p ${ROCKSDB_INCLUDES}
  - mkdir -p ${R_LIBS}
  - mkdir -p ${R_CHECK_DIR}
  - apt-get update
  - apt-get install -y libcurl4-openssl-dev libssl-dev libxml2-dev libicu-dev
  - apt-get install -y libgflags-dev zlib1g-dev libbz2-dev liblz4-dev libzstd-dev libsnappy-dev
  - apt-get install -y git-core
  - printf "options(Ncpus = %d)\n" $(nproc --all) >> ~/.Rprofile
  - install2.r -l ${R_LIBS} -s remotes
  - install2.r -l ${R_LIBS} -s $(r -e "${R_PKG_DEPS}")

build:rocksdb:
  extends: .build:rocksdb
  image: ubuntu:20.04

test:release:
  extends: .test:rpkg
  image: rocker/r-ver:latest
  dependencies:
    - build:rocksdb
  needs:
    - build:rocksdb

test:devel:
  extends: .test:rpkg
  image: rocker/r-ver:devel
  dependencies:
    - build:rocksdb
  needs:
    - build:rocksdb

lint:
  stage: deploy
  image: rocker/r-ver:latest
  when: on_success
  allow_failure: true
  script:
    - install2.r -l ${R_LIBS} -s lintr
    - r -e 'lintr::lint_package()'
  needs:
    - test:release

coverage:
  stage: deploy
  image: rocker/r-ver:latest
  when: on_success
  allow_failure: true
  script:
    - install2.r -l ${R_LIBS} -s covr
    - r -e 'covr::codecov(type = "all", quiet = FALSE)'
  dependencies:
    - build:rocksdb
  needs:
    - build:rocksdb
    - test:release
  coverage: '/rocksdb Coverage: \d+\.\d+\%/'

pages:
  stage: deploy
  image: rocker/r-ver:latest
  script:
    - apt-get install -y pandoc pandoc-citeproc
    - install2.r -l ${R_LIBS} -s drat pkgdown pkgload
    - r -e 'pkgdown::build_site(devel = TRUE)'
    - cp -a pkgdown/favicon/* public/
    - R CMD build ${R_BUILD_ARGS} .
    - mkdir -p public/src/contrib
    - r -e 'drat::insertPackage(argv, "public")' $(ls -1t *.tar.gz | head -n 1)
    #- install2.r -l ${R_LIBS} -s covr DT htmltools
    #- r -e 'covr::report(file = "public/coverage.html")'
  dependencies:
    - build:rocksdb
  needs:
    - build:rocksdb
    - test:release
  artifacts:
    paths:
      - public
