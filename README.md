
<!-- README.md is generated from README.Rmd. Please edit that file -->

# rocksdb

<!-- badges: start -->

[![GitLab CI Build
Status](https://gitlab.com/artemklevtsov/rocksdb/badges/master/pipeline.svg)](https://gitlab.com/artemklevtsov/rocksdb/pipelines)
[![Codecov Code
Coverage](https://codecov.io/gl/artemklevtsov/rocksdb/branch/master/graph/badge.svg)](https://codecov.io/gl/artemklevtsov/rocksdb)
[![License](http://img.shields.io/badge/license-GPL%20%28%3E=%203%29-brightgreen.svg?style=flat)](http://www.gnu.org/licenses/gpl-2.0.html)
[![CRAN
Status](http://www.r-pkg.org/badges/version/rocksdb)](https://cran.r-project.org/package=rocksdb)
[![Lifecycle:
experimental](https://img.shields.io/badge/lifecycle-experimental-orange.svg)](https://www.tidyverse.org/lifecycle/#experimental)
<!-- badges: end -->

RocksDB is an embeddable persistent key-value store for fast storage.

## Installation

You can install the development version of rocksdb from
[GitLab](https://gitlab.com/artemklevtsov/rocksdb) with:

``` r
install.packages("rocksdb", repos = "https://artemklevtsov.gitlab.io/rocksdb")
```

or with `remotes` package:

``` r
remotes::install_gitlab("artemklevtsov/rocksdb")
```

To install from the command line:

``` bash
git clone https://gitlab.com/artemklevtsov/rocksdb
cd rocksdb
R CMD INSTALL .
```

### Link with external rocksdb library

By defult the configure script try to find the system libraries and use
it. If rocksdb shared library not found it will be compiled from source
during package installation process.

To use already installed rocksdb shared library define `ROCKSDB_LIBS`
directory which contains the `librocksdb.so` file. Also you can use an
alternatuve location of the rocksdb header files.

``` r
Sys.setenv(ROCKSDB_INCLUDES = "/usr/local/include")
Sys.setenv(ROCKSDB_LIBS = "/usr/local/lib")
install.packages("rocksdb", repos = "https://artemklevtsov.gitlab.io/rocksdb")
```

or the same in terminal:

``` bash
export ROCKSDB_INCLUDES=/usr/local/include
export ROCKSDB_LIBS=/usr/local/lib
git clone https://gitlab.com/artemklevtsov/rocksdb
cd rocksdb
R CMD INSTALL .
```

## Usage

``` r
example("RocksDB", package = "rocksdb")
#> 
#> RcksDB> # path to database
#> RcksDB> db_path <- tempfile()
#> 
#> RcksDB> # create databasse
#> RcksDB> db <- RocksDB$new(db_path)
#>  list()
#> 
#> RcksDB> # print connection object
#> RcksDB> print(db)
#> <RocksDB>
#>   path: /tmp/RtmpvPZ2dN/file138f3dfff5b2 
#>   status: connected 
#> 
#> RcksDB> # get database path
#> RcksDB> db$path
#> [1] "/tmp/RtmpvPZ2dN/file138f3dfff5b2"
#> 
#> RcksDB> dir.exists(db$path) # TRUE
#> [1] TRUE
#> 
#> RcksDB> # number of keys
#> RcksDB> db$size()
#> [1] 0
#> 
#> RcksDB> # close connestion
#> RcksDB> # put value
#> RcksDB> db$put("cars", datasets::mtcars)
#> qs v.0.19.1
#> 
#> RcksDB> # put multiple values
#> RcksDB> db$mput(iris = datasets::iris, string = "Test string")
#> 
#> RcksDB> # get value
#> RcksDB> identical(db$get("cars"), datasets::mtcars)
#> [1] TRUE
#> 
#> RcksDB> # get multiple values
#> RcksDB> utils::str(db$mget(c("cars", "iris", "unknown")))
#> List of 3
#>  $ cars   :'data.frame': 32 obs. of  11 variables:
#>   ..$ mpg : num [1:32] 21 21 22.8 21.4 18.7 18.1 14.3 24.4 22.8 19.2 ...
#>   ..$ cyl : num [1:32] 6 6 4 6 8 6 8 4 4 6 ...
#>   ..$ disp: num [1:32] 160 160 108 258 360 ...
#>   ..$ hp  : num [1:32] 110 110 93 110 175 105 245 62 95 123 ...
#>   ..$ drat: num [1:32] 3.9 3.9 3.85 3.08 3.15 2.76 3.21 3.69 3.92 3.92 ...
#>   ..$ wt  : num [1:32] 2.62 2.88 2.32 3.21 3.44 ...
#>   ..$ qsec: num [1:32] 16.5 17 18.6 19.4 17 ...
#>   ..$ vs  : num [1:32] 0 0 1 1 0 1 0 1 1 1 ...
#>   ..$ am  : num [1:32] 1 1 1 0 0 0 0 0 0 0 ...
#>   ..$ gear: num [1:32] 4 4 4 3 3 3 3 4 4 4 ...
#>   ..$ carb: num [1:32] 4 4 1 1 2 1 4 2 2 4 ...
#>  $ iris   :'data.frame': 150 obs. of  5 variables:
#>   ..$ Sepal.Length: num [1:150] 5.1 4.9 4.7 4.6 5 5.4 4.6 5 4.4 4.9 ...
#>   ..$ Sepal.Width : num [1:150] 3.5 3 3.2 3.1 3.6 3.9 3.4 3.4 2.9 3.1 ...
#>   ..$ Petal.Length: num [1:150] 1.4 1.4 1.3 1.5 1.4 1.7 1.4 1.5 1.4 1.5 ...
#>   ..$ Petal.Width : num [1:150] 0.2 0.2 0.2 0.2 0.2 0.4 0.3 0.2 0.2 0.1 ...
#>   ..$ Species     : Factor w/ 3 levels "setosa","versicolor",..: 1 1 1 1 1 1 1 1 1 1 ...
#>  $ unknown: NULL
#> 
#> RcksDB> # check key exists
#> RcksDB> db$exists("cars") # TRUE
#> [1] TRUE
#> 
#> RcksDB> db$exists("unknown") # FALSE
#> [1] FALSE
#> 
#> RcksDB> # check multiple key exists
#> RcksDB> db$mexists(c("cars", "iris", "unknown"))
#> [1]  TRUE  TRUE FALSE
#> 
#> RcksDB> # delete entry
#> RcksDB> db$del("cars")
#> 
#> RcksDB> # delete multiple keys
#> RcksDB> db$mdel(c("iris", "string"))
#> 
#> RcksDB> # check database size
#> RcksDB> db$size() # 0
#> [1] 0
#> 
#> RcksDB> # make checkpoint
#> RcksDB> chk_path <- tempfile()
#> 
#> RcksDB> db$checkpoint(chk_path)
#> 
#> RcksDB> dir.exists(chk_path) # TRUE
#> [1] TRUE
#> 
#> RcksDB> # close connection
#> RcksDB> db$close()
#> 
#> RcksDB> # remove database direcotry
#> RcksDB> db$destroy()
#> 
#> RcksDB> dir.exists(db$path) # FALSE
#> [1] FALSE
```

## Bug reports

Use the following command to go to the page for bug report submissions:

``` r
bug.report(package = "rocksdb")
```

Before reporting a bug or submitting an issue, please do the following:

  - Make sure that no error was found and corrected previously
    identified. You can use the search by the bug tracker;
  - Check the news list for the current version of the package. An error
    it might have been caused by changes in the package. This can be
    done with `news(package = "rocksdb", Version ==
    packageVersion("rocksdb"))` command;
  - Make a minimal reproducible example of the code that consistently
    causes the error;
  - Make sure that the error triggered in the function from the
    `rocksdb` package, rather than in the code that you pass, that is
    other functions or packages;
  - Try to reproduce the error with the last development version of the
    package from the git repository.

When submitting a bug report please include the output produced by
functions `traceback()` and `sessionInfo()`. This may save a lot of
time.

## License

The `rocksdb` package is distributed under
[GPLv3](http://www.gnu.org/licenses/gpl-3.0.html) license.
