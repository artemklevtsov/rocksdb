#include <Rcpp.h>
#include <rocksdb/db.h>
#include <rocksdb/utilities/backupable_db.h>
#include "rocksdb_types.h"

using namespace Rcpp;
namespace rdb = rocksdb;

// [[Rcpp::export(rng=false)]]
BackupInfo db_backup_create(
  const std::string& backup_path,
  DBXPtr db,
  const std::string& metadata = ""
) {
  rdb::Status st;
  rdb::BackupEngine* eng;
  rdb::BackupableDBOptions opts(backup_path);
  st = rdb::BackupEngine::Open(rdb::Env::Default(), opts, &eng);
  if (!st.ok()) {
    stop("Can't init backup engine:\n%s", st.ToString());
  }
  st = eng->CreateNewBackupWithMetadata(db, metadata);
  if (!st.ok()) {
    stop("Can't create backup:\n%s", st.ToString());
  }
  std::vector<rdb::BackupInfo> info;
  eng->GetBackupInfo(&info);

  delete eng;
  return info;
}

// [[Rcpp::export(rng=false)]]
bool db_backup_delete(const std::string& backup_path, unsigned long id) {
  rdb::Status st;
  rdb::BackupEngine* eng;
  rdb::BackupableDBOptions opts(backup_path);
  st = rdb::BackupEngine::Open(rdb::Env::Default(), opts, &eng);
  if (!st.ok()) {
    stop(st.ToString());
  }
  st = eng->DeleteBackup(id);
  if (!st.ok()) {
    stop("Can't delete backup:\n%s", st.ToString());
  }
  return st.ok();
}

// [[Rcpp::export(rng=false)]]
BackupInfo db_backup_info(const std::string& backup_path) {
  rdb::Status st;
  rdb::BackupEngineReadOnly* eng;
  rdb::BackupableDBOptions opts(backup_path);
  st = rdb::BackupEngineReadOnly::Open(rdb::Env::Default(), opts, &eng);
  if (!st.ok()) {
    stop(st.ToString());
  }
  std::vector<rdb::BackupInfo> info;
  eng->GetBackupInfo(&info);
  delete eng;
  return info;
}

// [[Rcpp::export(rng=false)]]
bool db_backup_purge(const std::string& backup_path, std::size_t n) {
  rdb::Status st;
  rdb::BackupEngine* eng;
  rdb::BackupableDBOptions opts(backup_path);
  st = rdb::BackupEngine::Open(rdb::Env::Default(), opts, &eng);
  if (!st.ok()) {
    stop(st.ToString());
  }
  st = eng->PurgeOldBackups(n);
  if (!st.ok()) {
    stop("Can't purge backuup:\n%s", st.ToString());
  }
  return st.ok();
}

// [[Rcpp::export(rng=false)]]
bool db_backup_restore(
  const std::string& backup_path,
  const std::string& db_path,
  unsigned long id = 0
) {
  rdb::Status st;
  rdb::BackupEngineReadOnly* eng;
  rdb::BackupableDBOptions opts(backup_path);
  st = rdb::BackupEngineReadOnly::Open(rdb::Env::Default(), opts, &eng);
  if (!st.ok()) {
    stop(st.ToString());
  }
  if (id == 0) {
    st = eng->RestoreDBFromLatestBackup(db_path, db_path);
  } else {
    st = eng->RestoreDBFromBackup(id, db_path, db_path);
  }
  if (!st.ok()) {
    stop("Can't restore backup:\n%s", st.ToString());
  }

  delete eng;
  return st.ok();
}

// [[Rcpp::export(rng=false)]]
bool db_backup_verify(const std::string& backup_path, unsigned long id) {
  rdb::Status st;
  rdb::BackupEngineReadOnly* eng;
  rdb::BackupableDBOptions opts(backup_path);
  st = rdb::BackupEngineReadOnly::Open(rdb::Env::Default(), opts, &eng);
  if (!st.ok()) {
    stop(st.ToString());
  }
  st = eng->VerifyBackup(id);
  if (!st.ok()) {
    stop("Can't verify backup:\n%s", st.ToString());
  }
  return st.ok();
}
