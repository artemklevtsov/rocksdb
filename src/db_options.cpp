#include <Rcpp.h>
#include <rocksdb/db.h>
#include <rocksdb/utilities/convenience.h>
#include <rocksdb/utilities/options_util.h>
#include "utils.h"
#include "wrap.h"
#include "rocksdb_types.h"

using namespace Rcpp;
namespace rdb = rocksdb;

// [[Rcpp::export(rng=false)]]
OptsXPtr db_options_default() {
  rdb::Options opts;
  OptsXPtr ptr(new rdb::Options(opts));
  return ptr;
}

// [[Rcpp::export(rng=false)]]
std::string db_latest_options_file(const std::string& db_path) {
  rdb::Status st;
  rdb::Options opts;
  std::string res;
  st = rdb::GetLatestOptionsFileName(db_path, opts.env, &res);
  if (!st.ok()) {
    stop("Get latest config file:\n%s", st.ToString());
  }
  return db_path + "/" + res;
}

// [[Rcpp::export(rng=false)]]
OptsXPtr db_options_from_list(List x) {
  // return default options if input vector is empty
  if (x.size() == 0) {
    return OptsXPtr(new rdb::Options);
  }
  auto params = as<std::unordered_map<std::string, std::string>>(x);
  rdb::Status st;
  rdb::ColumnFamilyOptions cf_defualt;
  rdb::ColumnFamilyOptions cf_opts;
  st = rdb::GetColumnFamilyOptionsFromMap(cf_defualt, params, &cf_opts, false, true);
  if (!st.ok()) {
    warning("Parse ColumnFamilyOptions:\n%s", st.ToString());
  }

  rdb::DBOptions db_defualt;
  rdb::DBOptions db_opts;
  st = rdb::GetDBOptionsFromMap(db_defualt, params, &db_opts, false, true);
  if (!st.ok()) {
    warning("Parse DBOptions:\n%s", st.ToString());
  }

  rdb::Options opts(db_opts, cf_opts);
  OptsXPtr ptr(new rdb::Options(opts));
  return ptr;
}

// [[Rcpp::export(rng=false)]]
OptsXPtr db_options_from_file(const std::string& conf_file) {
  rdb::Status st;
  rdb::Options opts;
  std::vector<rdb::ColumnFamilyDescriptor> cf_descs;
  st = rdb::LoadOptionsFromFile(conf_file, opts.env, &opts, &cf_descs);
  if (!st.ok()) {
    stop("Can't load file '%s':\n%s", conf_file, st.ToString());
  }
  OptsXPtr ptr(new rdb::Options(opts));
  return ptr;
}

// [[Rcpp::export(rng=false)]]
rocksdb::Options db_options_to_list(OptsXPtr options) {
  return *options;
}
