#include <rocksdb/db.h>
#include "utils.h"

using namespace Rcpp;
namespace rdb = rocksdb;

// [[Rcpp::export(rng=false)]]
std::string write_sst(
  const std::string& sst_file,
  StringVector keys,
  List values
) {
  if (keys.size() != values.size()) {
    stop("'keys' and 'values' must be same length.");
  }

  rdb::Status st;
  rdb::Options opts;
  rdb::SstFileWriter writer(rdb::EnvOptions(), opts);
  writer.Open(sst_file);
  if (!st.ok()) {
    stop("Error while opening file %s:\n%s", sst_file, st.ToString());
  }

  std::size_t n = keys.size();
  for (std::size_t i = 0; i < n; ++i) {
    rdb::Slice sl_key(keys[i]);
    rdb::Slice sl_val = sexp_to_slice(values[i]);
    st = writer.Put(sl_key, sl_val);
    if (!st.ok()) {
      writer.Finish();
      stop("Error while adding key %s:\n%s", sl_key.ToString(), st.ToString());
    }
  }
  st = writer.Finish();
  if (!st.ok()) {
    stop("Error while finishing file %s:\n%s", sst_file, st.ToString());
  }

  return sst_file;
}
