#include "wrap.h"
#include <Rcpp.h>
#include <rocksdb/db.h>
#include <rocksdb/options.h>
#include <rocksdb/utilities/convenience.h>
#include <rocksdb/utilities/backupable_db.h>
#include "utils.h"

// nocov start

namespace rdb = rocksdb;

namespace Rcpp {

template <>
std::unordered_map<std::string, std::string> as(SEXP x) {
  List xx = as<List>(x);
  if (!xx.hasAttribute("names")) {
    stop("'x' must be named list.");
  }
  std::unordered_map<std::string, std::string> res;
  StringVector keys = xx.names();
  std::size_t n = xx.size();
  for (std::size_t i = 0; i < n; ++i) {
    RObject tmp;
    switch(TYPEOF(xx[i])) {
      case INTSXP:
      tmp = as<IntegerVector>(xx[i]);
      break;
    case REALSXP:
      tmp = as<NumericVector>(xx[i]);
      break;
    case LGLSXP:
      tmp = as<IntegerVector>(xx[i]);
      break;
    case STRSXP:
      tmp = as<StringVector>(xx[i]);
      break;
    default:
      stop("Unsupported type '%s'.", type2name(xx[i]));
    }
    StringVector values = as<StringVector>(tmp);
    std::size_t val_n = values.size();
    if (val_n == 0) {
      continue;
    } else if (val_n == 1) {
      res.emplace(keys[i], values[0]);
    } else {
      res.emplace(keys[i], str_collapse(values, ":"));
    }
  }
  return res;
}

template <>
SEXP wrap(const std::vector<rdb::BackupInfo>& info) {
  std::size_t n = info.size();

  NumericVector backup_id(n);
  NumericVector size(n);
  IntegerVector files(n);
  newDatetimeVector timestamp(n);
  StringVector metadata(n);

  for (std::size_t i = 0; i < n; ++i) {
    backup_id[i] = info[i].backup_id;
    timestamp[i] = info[i].timestamp;
    size[i] = info[i].size;
    files[i] = info[i].number_files;
    if (info[i].app_metadata.empty()) {
      metadata[i] = NA_STRING;
    } else {
      metadata[i] = info[i].app_metadata;
    }
  }

  DataFrame res = DataFrame::create(
    Rcpp::Named("backup_id") = backup_id,
    Rcpp::Named("size") = size,
    Rcpp::Named("number_files") = files,
    Rcpp::Named("timestamp") = timestamp,
    Rcpp::Named("metadata") = metadata,
    Rcpp::Named("stringsAsFactors") = false
  );

  return res;
}

template <>
SEXP wrap(const std::vector<rdb::CompressionType>& x) {
  std::size_t n = x.size();
  Rcpp::StringVector res(n);
  for (std::size_t i = 0; i < n; ++i) {
    std::string value;
    rdb::GetStringFromCompressionType(&value, x[i]);
    res[i] = value;
  }
  return res;
}

template <>
SEXP wrap(const std::vector<rdb::DbPath>& paths) {
  std::size_t n = paths.size();
  StringVector path(n);
  NumericVector size(n);
  for (std::size_t i = 0; i < n; ++i) {
    path[i] = paths[i].path;
    size[i] = paths[i].target_size;
  }
  DataFrame res = DataFrame::create(
    Rcpp::Named("path") = path,
    Rcpp::Named("size") = size
  );
  return res;
}

template <>
SEXP wrap(const rdb::Options& options) {
  std::string s;
  std::unordered_map<std::string,std::string> map;
  rdb::GetStringFromDBOptions(&s, options, ";");
  rdb::StringToMap(s, &map);
  rdb::GetStringFromColumnFamilyOptions(&s, options, ";");
  rdb::StringToMap(s, &map);
  Rcpp::List res = Rcpp::wrap(map);
  return res;
}

} // Rcpp

// nocov end
