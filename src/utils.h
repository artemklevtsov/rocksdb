#pragma once

#include <Rcpp.h>
#include <rocksdb/db.h>

std::string str_collapse(Rcpp::StringVector, const char*);
SEXP value_to_sexp(const std::string&);
rocksdb::Slice sexp_to_slice(SEXP);
rocksdb::Options load_options_file(const std::string&);
rocksdb::Options options_from_sexp(const Rcpp::StringVector&);
