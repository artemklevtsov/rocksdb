#include <Rcpp.h>
#include <rocksdb/db.h>
#include <rocksdb/version.h>
#include <rocksdb/utilities/convenience.h>
#include "rocksdb_types.h"

using namespace Rcpp;
namespace rdb = rocksdb;

// [[Rcpp::export(rng=false)]]
StringVector get_version() {
  std::stringstream ss;
  ss << std::to_string(ROCKSDB_MAJOR) << "."
     << std::to_string(ROCKSDB_MINOR) << "."
     << std::to_string(ROCKSDB_PATCH);
  StringVector res = StringVector::create(ss.str());
  res.attr("class") = "numeric_version";
  return res;
}

// [[Rcpp::export(rng=false)]]
StringVector get_supported_compressions() {
  return wrap(rdb::GetSupportedCompressions());
}
