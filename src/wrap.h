#pragma once

#include <RcppCommon.h>
#include <rocksdb/db.h>
#include <rocksdb/utilities/backupable_db.h>

namespace Rcpp {
template<> std::unordered_map<std::string, std::string> as(SEXP);
template<> SEXP wrap(const std::vector<rocksdb::BackupInfo>&);
template<> SEXP wrap(const std::vector<rocksdb::CompressionType>&);
template<> SEXP wrap(const std::vector<rocksdb::DbPath>&);
template<> SEXP wrap(const rocksdb::Options&);
} // Rcpp
