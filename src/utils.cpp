#include "utils.h"
#include <qs.h>
#include <rocksdb/db.h>

using namespace Rcpp;
using namespace qs;
namespace rdb = rocksdb;

// collapse to string with ':' delimiter
std::string str_collapse(StringVector x, const char* sep) {
  std::stringstream ss;
  std::copy(x.begin(), x.end(), std::ostream_iterator<const char*>(ss, sep));
  return ss.str();
}

SEXP value_to_sexp(const std::string& x) {
  RawVector raw(x.begin(), x.end());
  SEXP obj = c_qdeserialize(raw, false, true);
  return obj;
}

rdb::Slice sexp_to_slice(SEXP x) {
  RawVector raw = c_qserialize(x, "uncompressed", "uncompressed", 0, 0, true);
  rdb::Slice res(reinterpret_cast<const char*>(raw.begin()), raw.size());
  return res;
}
