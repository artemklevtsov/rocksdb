#include <Rcpp.h>
#include <rocksdb/db.h>
#include "utils.h"
#include "rocksdb_types.h"

using namespace Rcpp;
namespace rdb = rocksdb;

// [[Rcpp::export(rng=false)]]
BatchXPtr batch_create() {
  BatchXPtr ptr(new rdb::WriteBatch);
  return ptr;
}

// [[Rcpp::export(rng=false)]]
bool batch_destroy(BatchXPtr batch) {
  if (batch) {
    batch.release();
  };
  return batch.get() == nullptr;
}

// [[Rcpp::export(rng=false)]]
bool batch_clear(BatchXPtr batch) {
  batch->Clear();
  return true;
}

// [[Rcpp::export(rng=false)]]
bool batch_put(BatchXPtr batch, const char* key, SEXP value) {
  rdb::Slice sl_key(key);
  rdb::Slice sl_val(sexp_to_slice(value));
  rdb::Status st;
  st = batch->Put(sl_key, sl_val);
  if (!st.ok()) {
    stop("Can't write data to database:\n%s", st.ToString());
  }

  return st.ok();
}

// [[Rcpp::export(rng=false)]]
bool batch_del(BatchXPtr batch, const char* key) {
  rdb::Slice sl_key(key);
  rdb::Status st;
  st = batch->Delete(sl_key);
  if (!st.ok()) {
    stop("Can't write data to database:\n%s", st.ToString());
  }

  return st.ok();
}

// [[Rcpp::export(rng=false)]]
std::size_t batch_count(BatchXPtr batch) {
  return batch->Count();
}
