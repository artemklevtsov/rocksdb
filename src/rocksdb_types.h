#include <Rcpp.h>
#include <rocksdb/db.h>
#include <rocksdb/utilities/backupable_db.h>
#include <rocksdb/utilities/transaction_db.h>
#include <rocksdb/utilities/transaction.h>
#include "wrap.h"

typedef Rcpp::XPtr<rocksdb::DB> DBXPtr;
typedef Rcpp::XPtr<rocksdb::TransactionDB> TxnDBXPtr;
typedef Rcpp::XPtr<rocksdb::WriteBatch> BatchXPtr;
typedef Rcpp::XPtr<rocksdb::Options> OptsXPtr;
typedef Rcpp::XPtr<rocksdb::Transaction> TxnXPtr;
typedef std::vector<rocksdb::BackupInfo> BackupInfo;
