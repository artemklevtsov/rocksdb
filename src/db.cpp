#include <Rcpp.h>
#include <rocksdb/db.h>
#include <rocksdb/utilities/transaction_db.h>
#include <rocksdb/utilities/checkpoint.h>
#include <rocksdb/utilities/options_util.h>
#include <rocksdb/utilities/convenience.h>
#include "rocksdb_types.h"
#include "utils.h"

using namespace Rcpp;
namespace rdb = rocksdb;

// [[Rcpp::export(rng=false)]]
DBXPtr db_con(const std::string& path, OptsXPtr options, bool read_only = false) {
  rdb::DB* db;
  rdb::Status st;
  if (read_only) {
    st = rdb::DB::OpenForReadOnly(*options, path, &db);
  } else {
    st = rdb::DB::Open(*options, path, &db);
  }
  if (!st.ok()) {
    stop("Failed to open db:\n%s", st.ToString());
  }
  DBXPtr ptr(db);
  return ptr;
}

// [[Rcpp::export(rng=false)]]
TxnDBXPtr db_txn_con(const std::string& path, OptsXPtr options) {
  rdb::TransactionDB* db;
  rdb::TransactionDBOptions txn_opts;
  rdb::Status st;
  st = rdb::TransactionDB::Open(*options, txn_opts, path, &db);
  if (!st.ok()) {
    stop("Failed to open db:\n%s", st.ToString());
  }
  TxnDBXPtr ptr(db);
  return ptr;
}


// [[Rcpp::export(rng=false)]]
bool db_dcon(DBXPtr db) {
  // if already closed
  if (!db) {
    return false;
  }
  rdb::Status st;
  st = db->Close();
  db.release();
  return st.ok();
}

// [[Rcpp::export(rng=false)]]
bool db_destroy(const std::string& path) {
  rdb::Options opts;
  rdb::Status st;
  st = rdb::DestroyDB(path, opts);
  if (!st.ok()) {
    stop("Can't destroy db:\n%s", st.ToString());
  }
  return st.ok();
}

// [[Rcpp::export(rng=false)]]
bool db_add_sst(DBXPtr db, std::vector<std::string> sst_files) {
  rdb::Status st;
  rdb::IngestExternalFileOptions ifo;
  st = db->IngestExternalFile(sst_files, ifo);
  if (!st.ok()) {
    stop("Error while adding files to database: %s", st.ToString());
  }
  return st.ok();
}

// [[Rcpp::export(rng=false)]]
SEXP db_get(DBXPtr db, const char* key) {
  rdb::Slice sl_key(key);
  std::string value;
  rdb::Status st;
  rdb::ReadOptions opts;
  st = db->Get(opts, sl_key, &value);
  bool found = !st.IsNotFound();
  if (!found) {
    return R_NilValue;
  }
  if (found && !st.ok()) {
    stop("Can't get key from database:\n%s", st.ToString());
  }
  return value_to_sexp(value);
}

// [[Rcpp::export(rng=false)]]
List db_mget(DBXPtr db, StringVector keys) {
  std::size_t n = keys.size();
  std::vector<rdb::Status> st(n);
  std::vector<rdb::Slice> sl_keys(keys.begin(), keys.end());
  std::vector<std::string> values(n);
  rdb::ReadOptions opts;
  st = db->MultiGet(opts, sl_keys, &values);
  List res(n);
  for (std::size_t i = 0; i < n; ++i) {
    if (st[i].IsNotFound()) {
      res[i] = R_NilValue;
    } else {
      res[i] = value_to_sexp(values[i]); // deserilize
    }
  }
  res.names() = keys;
  return res;
}

// [[Rcpp::export(rng=false)]]
bool db_put(DBXPtr db, const char* key, SEXP value, bool sync = true) {
  rdb::Slice sl_key(key);
  rdb::Slice sl_val(sexp_to_slice(value));
  rdb::Status st;
  rdb::WriteOptions opts;
  opts.sync = sync;
  st = db->Put(opts, sl_key, sl_val);
  if (!st.ok()) {
    stop("Can't write data to database:\n%s", st.ToString());
  }

  return st.ok();
}

// [[Rcpp::export(rng=false)]]
bool db_mput(DBXPtr db, StringVector keys, List values, bool sync = true) {
  if (keys.size() != values.size()) {
    stop("'keys' and 'values' must be same length.");
  }
  std::size_t n = keys.size();
  // use batch to increase performance
  rdb::WriteBatch batch;
  for (std::size_t i = 0; i < n; ++i) {
    rdb::Slice sl_key(keys[i]);
    rdb::Slice sl_val(sexp_to_slice(values[i])); // serialize
    batch.Put(sl_key, sl_val);
  }
  rdb::WriteOptions opts;
  opts.sync = sync;
  rdb::Status st;
  st = db->Write(opts, &batch);
  if (!st.ok()) {
    stop("Can't write data to database:\n%s", st.ToString());
  }
  return st.ok();
}

// [[Rcpp::export(rng=false)]]
bool db_del(DBXPtr db, const char* key, bool sync = true) {
  rdb::WriteOptions opts;
  opts.sync = sync;
  rdb::Status st;
  rdb::Slice sl(key);
  st = db->Delete(opts, sl);
  if (!st.ok()) {
    stop("Can't delete key from db:\n%s", st.ToString());
  }
  return st.ok();
}

// [[Rcpp::export(rng=false)]]
bool db_mdel(DBXPtr db, StringVector keys, bool sync = true) {
  // use batch to increase performance
  rdb::WriteBatch batch;
  std::size_t n = keys.size();
  for (std::size_t i = 0; i < n; ++i) {
    rdb::Slice sl(keys[i]);
    batch.Delete(sl);
  }
  rdb::WriteOptions opts;
  opts.sync = sync;
  rdb::Status st;
  st = db->Write(opts, &batch);
  if (!st.ok()) {
    stop("Can't delet from keys db:\n%s", st.ToString());
  }
  return st.ok();
}

// [[Rcpp::export(rng=false)]]
bool db_exists(DBXPtr db, const char* key) {
  rdb::ReadOptions opts;
  std::string value;
  bool res = db->KeyMayExist(opts, key, &value);
  return res;
}

// [[Rcpp::export(rng=false)]]
LogicalVector db_mexists(DBXPtr db, StringVector keys) {
  std::size_t n = keys.size();
  LogicalVector res(no_init(n));
  rdb::ReadOptions opts;
  rdb::Iterator* it = db->NewIterator(opts);
  for (std::size_t i = 0; i < n; ++i) {
    rdb::Slice key(keys[i]);
    it->Seek(key);
    res[i] = it->Valid();
  }

  return res;
}

// [[Rcpp::export(rng=false)]]
bool db_write(DBXPtr db, BatchXPtr batch) {
  rdb::WriteOptions write_opts;
  rdb::Status st;
  st = db->Write(write_opts, batch);
  if (!st.ok()) {
    stop("Can't delet from keys db:\n%s", st.ToString());
  }
  return st.ok();
}

// [[Rcpp::export(rng=false)]]
bool db_checkpoint(DBXPtr db, const std::string& path) {
  rdb::Status st;
  rdb::Checkpoint* chk;
  st = rdb::Checkpoint::Create(db, &chk);
  if (!st.ok()) {
    stop("Can't create checkpoint:\n%s", st.ToString());
  }
  st = chk->CreateCheckpoint(path);
  delete chk;
  if (!st.ok()) {
    stop("Can't save checkpoint to %s:\n%s", path, st.ToString());
  }
  return st.ok();
}

// [[Rcpp::export(rng=false)]]
StringVector db_get_property(DBXPtr db, const StringVector& properties) {
  std::size_t n = properties.size();
  StringVector res(no_init(n));
  for (std::size_t i = 0; i < n; ++i) {
    std::string value;
    rdb::Slice sl(properties[i]);
    bool st = db->GetProperty(sl, &value);
    if (st) {
      res[i] = value;
    } else {
      res[i] = NA_STRING;
    }
  }
  return res;
}

// [[Rcpp::export(rng=false)]]
List db_get_options(DBXPtr db) {
  rdb::Options opts = db->GetOptions();
  std::string s;
  std::unordered_map<std::string,std::string> res;
  rdb::GetStringFromDBOptions(&s, opts, ";");
  rdb::StringToMap(s, &res);
  s.clear();
  rdb::GetStringFromColumnFamilyOptions(&s, opts, ";");
  rdb::StringToMap(s, &res);
  return wrap(res);
}

// [[Rcpp::export(rng=false)]]
bool db_set_options(DBXPtr db, const StringVector& params) {
  if (!params.hasAttribute("names")) {
    stop("'params' must be named vector.");
  }

  std::size_t n = params.size();
  StringVector keys = params.names();
  rdb::Status s;
  for (std::size_t i = 0; i < n; ++i) {
    std::string key = as<std::string>(keys[i]);
    std::string value = as<std::string>(params[i]);
    s = db->SetOptions({{key, value}});
    if (!s.ok()) {
      stop("Can't setup options to database:\n%s", s.ToString());
    }
  }
  return s.ok();
}

// [[Rcpp::export(rng=false)]]
std::vector<std::string> db_keys(DBXPtr db, const std::string& starts = "") {
  // StringVector res;
  std::vector<std::string> res;
  rdb::ReadOptions opts;
  rdb::Iterator* it = db->NewIterator(opts);
  for (it->SeekToFirst(); it->Valid(); it->Next()) {
    rdb::Slice key = it->key();
    if (!starts.empty() && !key.starts_with(starts)) {
      continue;
    }
    res.push_back(key.ToString());
  }

  if (!it->status().ok()) {
    stop("Can't get keys from database:\n%s", it->status().ToString());
  }

  return res;
}

// [[Rcpp::export(rng=false)]]
std::size_t db_keys_count(DBXPtr db, bool exact = false) {
  std::size_t res = 0;
  if (exact) {
    rdb::ReadOptions opts;
    rdb::Iterator* it = db->NewIterator(opts);
    for (it->SeekToFirst(); it->Valid(); it->Next()) {
      ++res;
    }
  } else {
    db->GetIntProperty("rocksdb.estimate-num-keys", &res);
  }

  return res;
}

// [[Rcpp::export(rng=false)]]
List db_to_list(DBXPtr db) {
  std::map<std::string, SEXP> res;
  rdb::ReadOptions opts;
  rdb::Iterator* it = db->NewIterator(opts);
  for (it->SeekToFirst(); it->Valid(); it->Next()) {
    rdb::Slice key(it->key());
    rdb::Slice value(it->value());
    res.insert({key.ToString(), value_to_sexp(value.ToString())});
  }
  if (!it->status().ok()) {
    stop("Can't get data from database:\n%s", it->status().ToString());
  }

  return wrap(res);
}
