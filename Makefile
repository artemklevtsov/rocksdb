PACKAGE := $(shell grep '^Package:' DESCRIPTION | cut -d ':' -f 2 | tr -d ' ')
VERSION := $(shell grep '^Version:' DESCRIPTION | cut -d ':' -f 2 | tr -d ' ')
TARBALL := $(PACKAGE)_$(VERSION).tar.gz
CHECK_DIR = $(PACKAGE).Rcheck
BUILD_ARGS := --no-build-vignettes --no-manual
CHECK_ARGS := --no-manual --as-cran
INSTALL_ARGS := --preclean --clean --strip
RCMD := $(shell which R) CMD
Rexpr := $(shell which Rscript) -e
export _R_CHECK_CRAN_INCOMING_ := false
export _R_CHECK_FORCE_SUGGESTS_ := true

.PHONY: doc build compile install check test vignette

all: build install README.md doc

compile:
	$(RCMD) INSTALL $(INSTALL_ARGS)  .

$(TARBALL): build

build:
	$(RCMD) build $(BUILD_ARGS) .

check: $(TARBALL)
	$(RCMD) check $(CHECK_ARGS) $<

install: $(TARBALL)
	$(RCMD) INSTALL $(INSTALL_ARGS) $<

doc: install
	$(Rexpr) 'roxygen2::roxygenise()'

test:
	$(Rexpr) 'tinytest::build_install_test()'

coverage:
	$(Rexpr) 'covr::package_coverage(type = "all", quiet = FALSE)'

manual: doc
	$(RCMD) Rd2pdf --no-preview --force -o $(PACKAGE)_$(VERSION)-manual.pdf .

README.md: README.Rmd
	$(Rexpr) 'rmarkdown::render("$<")'

clean:
	rm -f $(TARBALL)
	rm -rf $(CHECK_DIR)
	rm -f src/*.o
	rm -f src/*.so
	rm -f src/*.dll
	rm -f vignettes/*.md
	rm -f vignettes/*.html
	rm -f vignettes/*.pdf
	rm -f README.html
	find . -name '.Rhistory' -delete
	$(MAKE) -C src/rocksdb clean
	$(MAKE) -C src/lz4/lib clean
	$(MAKE) -C src/zstd/lib clean
	rm -f src/snappy/*.o
	rm -f src/snappy/*.a
