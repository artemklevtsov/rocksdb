library(tinytest)


## ---- source helpers ----

source("setup.R")


## ---- transaction database mget/mput/mdel methods ----

# create database
db_path <- tempfile()
con <- rocksdb:::db_txn_con(db_path, db_opts)

values <- list(
  integer = 0L,
  double = 0.0,
  string = "string",
  Date = Sys.Date(),
  POSIXct = Sys.time(),
  matrix = matrix(0, nrow = 10, ncol = 10),
  data.frame = datasets::mtcars
)
keys <- names(values)

for (i in seq_along(values)) {
  expect_true(rocksdb:::db_put(con, keys[i], values[[i]]))
  expect_true(rocksdb:::db_exists(con, keys[i]))
  expect_equal(rocksdb:::db_get(con, keys[i]), values[[i]])
  expect_true(rocksdb:::db_del(con, keys[i]))
  expect_null(rocksdb:::db_get(con, keys[i]))
  expect_false(rocksdb:::db_exists(con, keys[i]))
}

expect_null(rocksdb:::db_get(con, "unknown"))
expect_false(rocksdb:::db_exists(con, "unknown"))

# database destroy
rocksdb:::db_dcon(con)
rocksdb:::db_destroy(db_path)


## ---- transaction database mget/mput/mdel methods ----

# create database
db_path <- tempfile()
con <- rocksdb:::db_txn_con(db_path, db_opts)

values <- list(
  integer = 0L,
  double = 0.0,
  string = "string",
  Date = Sys.Date(),
  POSIXct = Sys.time(),
  matrix = matrix(0, nrow = 10, ncol = 10),
  data.frame = datasets::mtcars
)
keys <- names(values)

expect_true(rocksdb:::db_mput(con, keys, values))
expect_equal(rocksdb:::db_mget(con, keys), values)
expect_true(all(rocksdb:::db_mexists(con, keys)))
expect_true(rocksdb:::db_mdel(con, keys))
expect_false(any(rocksdb:::db_mexists(con, keys)))
expect_equal(rocksdb:::db_mget(con, "unknown"), list("unknown" = NULL))
expect_false(rocksdb:::db_mexists(con, "unknown"))

# database destroy
rocksdb:::db_dcon(con)
rocksdb:::db_destroy(db_path)
